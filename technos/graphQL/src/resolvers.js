const messageHello = "yeah ! hello man ";

const resolvers = {
  Query: {
    hello: () => messageHello,
  },
};

export default resolvers;
