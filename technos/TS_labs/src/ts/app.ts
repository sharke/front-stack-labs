let A: number = 321;
console.log("ts 002 >>", A);

let i = 0;
// let compteurBtn = document.querySelector("#compteurBtn") as HTMLButtonElement;
let compteurBtn = <HTMLButtonElement>document.querySelector("#compteurBtn");

const increment = () => {
  let span = document.querySelector("span");
  i++;
  span.innerText = i.toString();
  if (span) {
    span.innerText = i.toString();
  }
};

compteurBtn.addEventListener("click", increment);

// Syntaxe de base
const user: { name?: string; age?: number } = {};
const employer: { [key: string]: string } = { name: "test" };
const day: Date = new Date();

// Type Narrowing:
function printId(id: string | number) {
  if (typeof id === "string") {
    console.log("Narrowing to string >> ", typeof id);
  } else {
    console.log("Narrowing to number >> ", typeof id);
  }
}

//
function doSamTask(a: string | number | boolean, b: string | Date | object) {
  if (a === b) {
    console.log("a >> ", a); // a est narrowé a string par deduction depuis la condition ( a === b ) par les type en commun entre a et b !!
    console.log("b >> ", b); // b est narrowé a string par deduction depuis la condition ( a === b ) par les type en commun !!
  }
}

// Alias & Generics

type User = { firstName: string; lastName: string };
type employer = typeof user;
const mrX: User = { firstName: "joe", lastName: "xard" };

function identity<T>(arg: T): T {
  return arg;
}

const a = identity<number>(6); //  identity<number>(arg: number): number

function consoleSize<T extends { length: number }>(arg: T): T {
  console.log(arg.length);
  return arg;
}

type identity<T> = (arg: T) => T;

type P = keyof User;

const rr: P = "lastName";

const X = consoleSize(["aa", "bb", "cc"]);
console.log("X ", X);

// Les classes en TypeScript:
function reversing<T>(Arr: readonly T[]): T[] {
  return [...Arr].reverse(); // faut copier le tableau pour que ça marche
}

class A1 {
  protected a: string = "va";
}

class B1 extends A1 {
  log() {
    console.log(this.a);
  }
}

let bb = new B1();
bb.log(); // les methode de la class peuvent acceder a la valeur de a

// Type ou Interface ?

interface Point {
  x: number;
  y: number;
  showXY(): string;
}

class coordonnee implements Point {
  x: number;
  y: number;
  showXY(): string {
    return ` x = ${this.x} | et y = ${this.y}`;
  }
}

interface Window {
  googleAnalytics: string;
}

let Win: Window;
Win.googleAnalytics = "Code-google-analytics";

// les tuples

type ListItem = [string, number]; // type tuple de 2 casa avec la 1er est string et la deusiemme est number

const tuple1: ListItem = ["tomate", 2];
const tuple2: ListItem = ["courgette", 6];

function merge<T extends unknown[], U extends unknown[]>(
  a: T,
  b: U
): [...T, ...U] {
  return [...a, ...b];
}

const res = merge(tuple1, tuple2); // res aura le type [ string , number , string , number]
const res2 = merge(tuple1, [1, 2, 3]); // res2 aura le type [ string , number , ...number[]]

//---------------- GLOBAL EXECUTION TESTS ----------------------//
function main(): void {
  console.log("main");
  printId("4");
}

main();
